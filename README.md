# Amazon Cloudwatch API Connector
"Amazon CloudWatch monitors your Amazon Web Services (Amazon Web Services) resources and the applications you run on Amazon Web Services in real time."

Documentation: https://docs.aws.amazon.com/monitoring/

Specification: https://github.com/APIs-guru/openapi-directory/blob/main/APIs/amazonaws.com/monitoring/2010-08-01/openapi.yaml

## Prerequisites

+ An Amazon Cloudwatch account
+ Generate an AWS Acess Key ID and AWS Secret Key from the AWS Admin Console

## Supported Operations
All endpoints are operational.
## Connector Feedback

Feedback can be provided directly to Product Management in our [Product Feedback Forum](https://community.boomi.com/s/ideas) in the boomiverse.  When submitting an idea, please provide the full connector name in the title and a detailed description.

